### In Plesk, execute with:
### bash _deploy.sh >> _deployment.log 2>&1

### To update TinyTeX and the LaTeX packages:
### tinytex::install_tinytex();
### tinytex::tlmgr_update();

echo -----------------------------------------
echo Starting at: $(date)

### Go to directory with cloned git repo
cd ~/deploy_spark.opens.science

### Set the path so LaTeX can be found
PATH=$PATH:/var/www/vhosts/sysrevving.com/.phpenv/shims:/opt/plesk/phpenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/var/www/vhosts/sysrevving.com/.TinyTeX/bin/x86_64-linux

### Delete old 'public' directory if it exists
#rm -rf public

### Potentially install latex packages
# tinytex::tlmgr_install('koma-script')
# tinytex::tlmgr_install('bookmark')
# tinytex::tlmgr_install('caption')
# tinytex::tlmgr_install('tcolorbox')
# tinytex::tlmgr_install('pgf')
# tinytex::tlmgr_install('environ')
# tinytex::tlmgr_install('pdfcol')

echo Current directory: $(pwd)
echo Current '$PATH': $PATH
echo Running Quarto

### Render the site
/usr/local/bin/quarto render --to all

echo Finished Quarto. Deleting old contents...

### Delete all contents in public HTML directory
rm -rf ~/spark.opens.science/*.*
rm -rf ~/spark.opens.science/*
rm -f ~/spark.opens.science/.htaccess

echo Deleted old contents. Copying new contents...

### Copy website
cp -RT public ~/spark.opens.science

### Copy .htaccess
cp .htaccess ~/spark.opens.science

echo Finished at: $(date)
echo -----------------------------------------
