# Triggers {#sec-triggers}
Professionals can configure triggers based on data acquired by SPARK. When a trigger is set on an activity, it will determine whether this activity will be shown to the participant or not.

Activities which have a trigger set on them, will periodically be evaluated on previously gathered sensor and survey data. If the trigger conditions are met, the activity will be shown to the participant.

## Instructions

<!-- Triggers can be set based on data from the following sensors:

- GPS (gps), with parameters:
  - longitude
  - latitude
  - radius
- pedometer (steps), with parameters:
  - steps -->

<!-- Note: the desired sensor must first be created in the "Sensors" section of the dashboard. -->

Triggers can be set based on the following question types: 

- Boolean
- List
- Multi-Select
- Slider
- Rating
- Time
- Likert
- Short Answer

Triggers are set in the "Decision Tree Builders" section of the dashboard.

### Visual builder
A professional can use a flowchart-like visual builder to construct triggers. 

#### Evaluation
An evaluation of survey/sensor data.

![](img/docs/visual-builder-evaluation.png){width=500 fig-align="left"}

#### Activity
Equivalent to the `StartSurvey(...)` method.

![](img/docs/visual-builder-activity.png){width=150 fig-align="left"}

#### Data Source
Equivalent to the `DataSource(...)` method.

![](img/docs/visual-builder-data-source.png){width=150 fig-align="left"}

#### Action
Not used.

![](img/docs/visual-builder-action.png){width=150 fig-align="left"}

### Logical expression
A professional can configure a trigger by logical expression.

![](img/docs/trigger-expression.png){width=500 fig-align="left"}

<!-- Currently, it is only possible to add a trigger based on data from a single sensor (as shown in the picture above). -->

#### Syntax
A trigger consists of 1 or more blocks. A block must be of the format: 

> { EXPRESSION, ACTION_TRUE, ACTION_FALSE }

- `EXPRESSION` is the expression to be evaluated. The syntax for the expressions is explained in @sec-expressions .</br>
- `ACTION_TRUE` defines the action that will be performed when the trigger is expression evaluates to true.</br>
- `ACTION_FALSE` defines the action that will be performed when the trigger is expression evaluates to false. This is optional.

The action could be starting a survey (`StartSurvey(...)`), setting a variable (`DataSource(...)`), or an integer. An integer refers to the index of one of the expression blocks. The expression blocks can be viewed as an array, which has a zero based index that auto increments. The first block has index 0, the second block has index 1, etc.

A block can also start with an action (e.g. `StartSurvey`), in this case the expression is automatically evaluated to true. `ACTION_TRUE` and `ACTION_FALSE` can also be used as previously defined in this case, for example:

> { StartSurvey(NewSurvey), 1 } {survey1.A CONTAINS a, StartSurvey(OtherSurvey) }


If the action is the index of one of the expression blocks, then the expression of this block will be evaluated and its action will be performed. This way expression blocks can be performed in a chain.

For example: 

> { (survey1.qA == 3) OR (survey1.qB == 2), 1, 2 } { survey1.vraagD == 7, StartSurvey(SurveyZ) } { spark.steps <= 1000, StartSurvey(SurveyF) }

If the first expression evaluates to true, the second block will be evaluated. Furthermore, if the expression in the second block evaluates to true, SurveyZ will be started.

Similarly, if the first expression evaluates to false, the third block will be evaluated. If the expression in this third block evaluates to true, SurveyF will be started.