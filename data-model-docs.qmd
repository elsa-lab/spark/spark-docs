# The Data Model of the SPARK

This shows the SPARK data model.

<!-- <img src="img/SPARK-data-model.drawio.svg" alt="SPARK Data model"/> -->
<img src="img/detailed-data-model.drawio.svg" alt="SPARK Data model" style="width:100%"/>

## Abstract Data Model

A more general representation of the SPARK platform.

<img src="img/abstract-data-model.drawio.svg" alt="abstract data model"/>
